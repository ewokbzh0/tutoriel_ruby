# Les classes et les objets
Vous savez, en Ruby on dit que tout est objet. Imaginez donc à quel point c'est compliqué de faire un tutoriel sur Ruby, sans trop évoquer les objets jusqu'au moment où on parle de classe.

## Les classes

Une classe en Ruby est un objet de type `Class`, ça sert à décrire des concepts. Par exemple, la classe `Array` décrit les tableaux ordonnés.

Une classe permet de donner à ses objets tout un ensemble de méthodes qui permettent de manipuler le concept. En Ruby, quand vous n'êtes pas au sein du concept vous ne pouvez le modifier qu'au travers de méthodes. Par défaut, toutes les méthodes que vous définirez seront aussi publiques, à moins que vous ne dites le contraire.

Une méthode publique est une méthode que l'on peut exécuter à l'aide de `objet.nom_methode`. Les méthodes privées, elles, ne peuvent être exécutées que par les méthodes de l'objet.

### Définir une classe

Pour définir une classe c'est assez simple, il suffit d'écrire le mot clé `class`, d'écrire le nom de la classe sous forme de constante et ensuite la décrire pour terminer par le mot clé `end`.

```ruby
class Point
  # Définition du concept de point
end
```

### Définir le constructeur d'une classe

Le constructeur d'une classe en Ruby est la méthode `initialize`. Quoi qu'il arrive elle sera privée (c'est une norme) donc vous n'aurez pas besoin de la définir comme étant privée. Les paramètres de `initialize` seront les paramètres que l'utilisateur devra passer à la méthode singleton `new` de la classe.

```ruby
# Class describing a 2D point
class Point2D
  # Create a new Point2D
  # @param x_pos [Integer] x position of the point
  # @param y_pos [Integer] y position of the point
  def initialize(x_pos, y_pos)
    @x = x_pos
    @y = y_pos
  end
end

point = Point2D.new(5, 6)
# point = #<Point2D:0x03268620 @x=5, @y=6>
```

Comme vous avez pu le voir, j'ai mis ce qu'on appelle des commentaires, mais ce ne sont pas n'importe quels commentaires. Ce ne sont pas des commentaires qui servent à expliquer à quelqu'un qui comprend rien au Ruby ce qu'on fait, ce sont des commentaires qui servent à documenter le code et générer des documentations automatiquement à l'aide de [YARD](https://yardoc.org). Je vous recommande vivement de faire de même, ceci vous simplifiera la vie et sera bénéfique pour vos futurs utilisateurs.

### Définir une méthode dans une classe

On a déjà défini `initialize`, le principe reste le même, la différence c'est que si vous voulez que des méthodes soient publiques et d'autres privées, il peut être nécessaire d'utiliser le mot clé `private` ou `public`

Exemple : 
```ruby
class Point2D # Pas besoin de rappeler la doc à chaque fois.
  
  # Convert an input value to a Integer
  # @param value [Numeric, String]
  # @return [Integer] 
  private def safe_to_i(value)
    return value.to_i if value.is_a?(Numeric) || value.is_a?(String)
    return value.to_s.to_i
  end

  # Assign x to a new value
  # @param x_pos [Integer] new value of x
  def x=(x_pos)
    @x = safe_to_i(x_pos)
  end
end
```

Dans cet exemple, j'ai mis le mot clé `private` juste avant le mot `def` sur la même ligne. Ca indique à ruby que cette méthode est privée.

Pour la méthode `x=` (qui permet d'écrire `point.x = value`) je n'ai rien mis car par défaut les méthodes définies sont publiques, sauf si on a changé le contexte de définition.

Par exemple :
```ruby
# Some random class
class RandomClass
  private

  # First private method
  def first_private_method
  end

  # Second private method
  def second_private_method
  end

  # First method of RandomClass that is public
  public def first_public_method
  end
end
```

Quand les mots clés `private` ou `public` sont mis seuls sur une ligne dans la définition d'une classe, ils indiquent à ruby que tout ce qui suit est privé ou publique (sauf exception ou changement de contexte).

### Les attributs

En Ruby il est possible de définir des attributs, c'est simplement des méthodes d'accès rapides aux variables d'instance d'un objet.

Il existe 3 types d'attributs :
* `attr_reader` : Permet de définir un attribut en lecture seule.
* `attr_writer` : Permet de définir un attribut en écriture seule.
* `attr_accessor` : Permet de définir un attribut en lecture écriture.

Il est recommandé de toujours écrire soi-même les méthodes d'attributs en écriture car `attr_writer` et `attr_accessor` cassent l'encapsulation. Si on écrit n'importe quoi dans les variables d'instance on se retrouve avec des bugs.

Exemple :
```ruby
class Point2D
  # @return [Integer] x coordinate of the point
  attr_reader :x
end
```

Quand on écrira `point.x` on lira la variable `@x` du `Point2D`.

### Définir une classe complète

Maintenant que nous avons dit tout ça nous allons définir un concept de point en 2D avec un certain nombre d'outils :
* Pouvoir modifier les coordonnées d'un coup, ou l'une d'entre elles.
* Pouvoir déplacer le point
* Pouvoir calculer sa distance par rapport à un autre point
* Pouvoir copier les coordonnées d'un autre point

Pour ça, nous allons utiliser les attributs, des méthodes privées et de la vérification de types. Nous allons également utiliser un mot clé : `self` qui se rapporte à l'objet dans le contexte courant.

Le résultat se trouve dans [Point2D.rb](../scripts/Point2D.rb).

### L'héritage

L'héritage en programmation orientée objet est un concept assez pratique. Il permet d'améliorer d'autres concepts sans avoir à tout réécrire.

Les class héritant d'une classe parente ont accès à tout le savoir de la classe parente (méthodes, constantes etc...).

Pour spécifier la classe parente on utilise le signe `<` après le nom de la classe qu'on fait suivre par le nom de la classe parente :
```ruby
class Point3D < Point2D
end
```

Nous allons améliorer le concept de Point 2D pour en faire un Point 3D. 

Attention, en Ruby on ne peut hériter que d'une seule classe (pas d'héritage multiple).

Pour appeler la méthode de la classe parente lorsque l'on redéfinit celle-ci on utilise le mot clé `super`. Si on ne donne pas de paramètre, ça donne exactement les mêmes paramètres qui ont été passés à notre méthode. De ce fait, il est préférable d'utiliser les parenthèses quand vous voulez spécifier les paramètres.

Exemples :
```ruby
class Enfant < Parent
  def method1(param1)
    super # Va envoyer param1
    # do_something
  end

  def method2(param1)
    super() # On envoie aucun paramètre
    # do_something
  end

  def method3(param1, param2)
    super(param1) # On n'envoie que param1
    # do_something
  end
end
```

Je vous ai fait le script [Point3D.rb](../scripts/Point3D.rb) pour vous montrer l'héritage.

## Les méthodes singleton

Les méthodes singleton sont des méthodes qui appartiennent à un objet. On ne les retrouve pas nécessairement dans d'autres objets. Ces méthodes se définissent d'une manière un peu particulière :

* Dans une autre méthode :

    ```ruby
    def nom_methode
      def methode_singleton
        # do something in the context of the returned object of nom_methode
      end
      # do stuff
      return an_object
    end
    ```

    Le retour de `nom_methode` donnera l'accès à `methode_singleton` qui est une méthode singleton de cet objet. Ca veut dire que `methode_singleton` a accès au contexte de la valeur de retour (comme si elle était définie dans sa classe) et peut être appelée comme si elle était définie.

* En utilisant la syntaxe `def obj.nom_methode`

    C'est ainsi qu'on reproduit plus ou moins les fonctions statiques en Ruby pour les classes :
    ```ruby
    class MaClasse
      # Ici self vaut MaClasse
      def self.methode_singleton
        # do stuff
      end
    end
    ```
    Après cette définition on pourra écrire `MaClasse.methode_singleton`.

Pour les classes il existe une autre syntaxe pour définir ces méthodes :
```ruby
class MaClasse
  class << self
    def methode_singleton
      # do stuff
    end
  end
end
```
Cette syntaxe est intéressante quand vous avez beaucoup de méthodes à définir.

### Les constantes

Les classes peuvent contenir des constantes (que ce soit une sous-classe ou une constante régulière).

Par convention on écrit les constantes qui ne sont pas des classes en majuscule et en séparant les mots par des underscores.

Exemple :
```ruby
class Point2D
  ORIGIN = new(0, 0).freeze
end
```

Ici nous avons enregistré l'origine du repère 2D dans la constante `ORIGIN`. Nous avons utilisé la méthode `freeze` pour être certain que l'origine ne sera pas modifiée (par `set_coordinates` par exemple).

Si vous voulez accéder à la constante `ORIGIN` depuis un contexte autre que l'intérieur de Point2D vous devrez écrire `Point2D::ORIGIN` pour lire la constante. Si vous êtes dans la définition de la classe vous n'écrirez que `ORIGIN`.

## Mot de la fin
J'ai décrit pas mal de choses pour les classes mais je n'ai pas fini car les modules me permettront d'introduire les Mixin (ce qui permet de palier à l'héritage multiple inexistant).