# Les tableaux associatifs

Les tableaux associatifs ou Hash en Ruby ont plusieurs utilités :
* Décrire simplement une donnée sans avoir à écrire le concept.
* Stocker les options d'une méthode
* Associer des éléments clés à des éléments valeurs (pour éviter d'écrire de longues suites de conditions).

### Créer un tableau associatif

Dans la partie [Les variables et les types de données](1_Variables_Et_Type_De_Donnees.md) nous avons vu comment littéralement créer un tableau associatif. Il existe une méthode `new` qui permet de préciser une information qui peut être importante : la valeur par défaut.

Par défaut, quand vous lisez une clé qui n'a pas de valeur dans le tableau associatif, la valeur retournée est nil. Il est possible de changer ça lors de la création :

```ruby
Hash.new(default_value)
```

Tout comme pour Array, la valeur par défaut n'est pas répliquée, il est donc préférable d'utiliser un bloc lors de la création :
```ruby
Hash.new { |hash, key| next(value) }
```

Le paramètre `hash` est le hash lui-même et le paramètre `key` est la clé que l'utilisateur a voulu obtenir. (Ceci permet de faire des petits calculs.)

Note importante : Si vous définissez un Hash à l'aide d'un bloc, il ne peut plus être sérialisé. Le systèmes de sérialisation comme JSON, YAML ou Marshal ne savent pas sérialiser du code.

### Lire la valeur associé à une clé

Pour les tableaux associatifs, les clés sont les index, de ce fait pour lire la valeur associée à une clé on utilise l'opérateur `[]`.

Exemple :
```ruby
hash = { nom: 'Yuri', age: 25 }
hash[:nom] # "Yuri"
hash[:age] # 25
```

### Modifier la valeur associée à une clé

Dans le même ordre d'idée, la modification se fait à l'aide de `[]=`.

Exemple :
```ruby
hash = { nom: 'Yuri', age: 24 }
hash[:age] = 25 
# hash = { :nom => "Yuri", :age => 25 }
```

### Vérifier la présence d'une clé

Pour les Hash la présence d'une clé se vérifie à l'aide de la méthode `key?`. La méthode `has_key?` est un alias.

Exemple :
```ruby
hash = { nom: 'Yuri', age: 25 }
hash.key?(:nom) # true
hash.key?(:taille) # false
```

### Vérifier la présence d'une valeur

De la même manière on peut vérifier la présence d'une valeur dans le tableau à l'aide de `value?`. Son alias est `has_value?`.

Exemple :
```ruby
hash = { nom: 'Yuri', age: 25 }
hash.value?('Yuri') # true
hash.value?(42) # false
```

### Supprimer une clé

Pour supprimer une clé du tableau associatif il suffit d'appeler la méthode `delete`.

Exemple:
```ruby
hash = { nom: 'Yuri', age: 25 }
hash.delete(:nom)
# hash = { :age => 25 }
```

### Connaître le nombre de clés présentes

Pour connaître le nombre de clés présentes, il suffit d'appeler `size` ou `length`.

Exemple :
```ruby
hash = { one: 1, two: 2, four: 4 }
hash.size # 3
```

### Extraire certaines associations clé valeur

La méthode `slice` permet d'extraire uniquement ce qui vous intéresse dans le tableau associatif.

Exemple :
```ruby
hash = { nom: 'Yuri', age: 25, taille: 950, localisation: 'mine' }
hash.slice(:nom, :age) # { :nom => 'Yuri', :age => 25 }
```

### Fusionner deux tableaux associatifs

Parfois vous désirez fusionner deux tableaux (informations complémentaires). Pour cela vous pouvez utiliser la méthode `merge` (crée un nouveau tableau) ou `merge!` (affecte le tableau actuel).

Note : Si deux clés identiques apparaissent dans les tableaux, c'est celle du tableau passé en paramètre qui sera choisie.

Exemple :
```ruby
hash1 = { nom: 'Yuri' }
hash2 = { age: 25 }
hash1.merge!(hash2)
# hash1 = { :nom => "Yuri", :age => 25 }
```

## L'exploration

Comme dans les tableaux ordonnés, il est parfois utile de pouvoir parcourir un tableau associatif. Pour cela il existe trois méthodes équivalentes à celles des tableaux ordonnés :
* `each_value` : parcourt toutes les valeurs du tableau associatif.
* `each` : parcourt tous les couples clé valeur du tableau associatif.
* `each_key` : parcourt toutes les clés du tableau associatif.

Exemples :
```ruby
hash = { nom: 'Yuri', age: 25 }
hash.each_value { |value| p value }
# affiche :
# "Yuri"
# 25
hash.each { |key, value| puts "hash[#{key.inspect}] = #{value.inspect}" }
# affiche :
# hash[:nom] = "Yuri"
# hash[:age] = 25
hash.each_key { |key| p key }
# affiche :
# :nom
# :age
```

Tout comme pour les tableaux, il existe des méthodes permettant de réaliser des traitements, je vous laisse parcourir la documentation pour cela.