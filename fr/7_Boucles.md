# Les boucles
Il est assez étrange de voir les boucles abordées après la définition de méthode, ça aurait été plus logique de les expliquer directement après les conditions. Mais si j'ai fait cela, c'est pour une raison assez simple. Il est assez rare que l'on utilise des boucles conventionnelles en Ruby. On appellera plutôt des méthodes qui prennent des blocs pour exécuter un traitement spécifique, c'est pourquoi j'ai expliqué les blocs juste avant.

## Les boucles conventionnelles

Il existe deux types de boucles conventionnelles en Ruby :
* `while` qui exécute son code tant que la condition est valide.
* `until` qui est l'opposée de `while`, elle exécute son code tant que la condition n'est pas valide.

Les boucles `while` et `until` peuvent également s'écrire comme les conditions, après l'assertion dans le but de faire une boucle sur une ligne.

Exemples :
```ruby
i = 0
while i < 5
  i = i + 1
end
# Fera, i = 1, i = 2, i = 3, i = 4, i = 5
i = 0
until i >= 5
  i = i + 1
end
# Fera, i = 1, i = 2, i = 3, i = 4, i = 5
i = 0
# Version compacte du premier while :
i += 1 while i < 5
i = 0
# Version compacte du premier until :
i += 1 until i >= 5
```

## Faire un do while
En Ruby le do while est pas beau parce qu'il s'écrit ainsi :
```ruby
i = 0
begin
  i += 1
end while i < 5
```

## La boucle for
Elle n'existe pas en Ruby. Enfin, c'est surtout un gigantesque mensonge.

Dans quasiment tous les langages de programmation, une boucle for c'est : une initialisation, une condition d'arrêt et une exécution permettant à terme d'arriver à la condition d'arrêt.

En Ruby, c'est une variable affectée, un objet pour lequel on va appeler la méthode `each`.

La boucle for s'écrit ainsi en Ruby :
```ruby
for variable in object_that_has_each_method
  # execute code
end
```

Exemple :
```ruby
for i in 0...5
  p i
end
# Affiche :
# 0
# 1
# 2
# 3
# 4
```

A la place de `for` on utilise des méthodes plus explicites (qui en plus ne nécessitent pas la création d'objet) ou alors on appelle directement la méthode `each`.

## Boucle de 0 à n exclu

Cette boucle est assez simple, on appelle la méthode `times` des nombres entiers.

Exemple :
```ruby
5.times { |i| p i }
# Affiche :
# 0
# 1
# 2
# 3
# 4
```

Si vous voulez utiliser cette méthode avec la taille d'un tableau, évitez, y'a mieux que ça :
```ruby
tableau = [1, 2]
tableau.each_index { |i| p i }
# Affiche :
# 0
# 1
```

## Boucle de x à y (inclut)

Ce type de boucle s'écrit de trois manières différentes :
* `x.upto(y)` si l'on veut aller de x à y (x <= y) de 1 en 1.
* `x.downto(y)` si l'on veut aller de x à y (x >= y) de -1 en -1.
* `x.step(y, pas)` si l'on veut aller de x à y avec un pas.

Exemples : 
```ruby
1.upto(2) { |i| p i }
# Affiche :
# 1
# 2
2.downto(1) { |i| p i }
# Affiche :
# 2
# 1
4.step(0, -2) { |i| p i }
# Affiche :
# 4
# 2
# 0
```

## Le contrôle dans les boucles

Dans les boucles vous avez trois mots clés qui vous permettent de contrôler votre boucle :
* `break` qui permet d'arrêter la boucle. Il est possible de donner un paramètre qui sera une valeur de retour de la boucle.

  ```ruby
  valeur = 5.times do |i|
    p i
    break # généralement avec une condition
  end
  # Affiche :
  # 0
  # valeur = nil # Aucun paramètre donné à break
  valeur = 5.times do |i|
    p i
    break(i)
  end
  # Affiche :
  # 0
  # valeur = 0
  ```

  Notez que par défaut, s'il n'y a pas de `break`, la valeur retournée par times est `self` (ici 5).

* `next` qui permet de sauter à l'itération suivante. Le paramètre de `next` est la valeur retournée par `yield` ou la méthode `call`.

  ```ruby
  5.times do |i|
    next if (i % 2).zero?
    p i
  end
  # Affiche :
  # 1
  # 3
  mon_proc = proc { |i| next(i * 5) }
  mon_proc.call(3) # 15
  ```

* `redo` qui permet de recommencer l'itération courante. (Pas de paramètre pour `redo`).

  ```ruby
  a = 0
  5.times do |i|
    if a != i
      puts "a = #{a}"
      a += 1
      redo
    end
    puts "i = #{i}"
    a = 0
  end
  # Affiche :
  # i = 0
  # a = 0
  # i = 1
  # a = 0
  # a = 1
  # i = 2
  # a = 0
  # a = 1
  # a = 2
  # i = 3
  # a = 0
  # a = 1
  # a = 2
  # a = 3
  # i = 4
  ```

## Les boucles infinies

En Ruby, vous pouvez créer des boucles infinies à l'aide de la méthode `loop`.

Exemple :
```ruby
loop do
  puts "J'vois pas sur les côtés j'vais tout droit !" # watch?v=x9LxHtbuhSA
end
# Affiche :
# J'vois pas sur les côtés j'vais tout droit !
# J'vois pas sur les côtés j'vais tout droit !
# J'vois pas sur les côtés j'vais tout droit !
# J'vois pas sur les côtés j'vais tout droit !
# ...
```

Il est intéressant d'utiliser les contrôles dans une boucle infinie.

## Mot de la fin
Nous avons vu les principales boucles en Ruby, après vous avez des sortes de boucles qui proviennent des `Enumerator` je vous conseille très vivement de regarder ces méthodes (`map`, `select`, `reject` etc...) elles peuvent vous éviter d'écrire des boucles peu optimisées et assez compliquées à comprendre.